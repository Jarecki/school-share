package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.FleetComposer;

public class FleetComposerShipCountTest {

	@Test
	public void test() {
		int[] ships = {1,2,3,4};
		FleetComposer fleetComposer = new FleetComposer(ships, 10, 10);
		assertEquals(fleetComposer.getShipCount(), 4);
	}

}
