package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.Ship;
import com.awpto.statki.ShipPrototype;

public class ShipLengthTest {

	@Test
	public void test() {
		Ship ship = new Ship(new ShipPrototype());
		assertEquals(ship.getLength(),1);
	}

}
