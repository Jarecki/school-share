package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.ui.FormationPanel;

public class TileSizeTest {

	@Test
	public void test() {
		FormationPanel fPanel = new FormationPanel();
		int result = fPanel.getTileSize();
		assertEquals(45, result);
	}

}
