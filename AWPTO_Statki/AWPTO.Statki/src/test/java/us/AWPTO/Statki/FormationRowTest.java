package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.Formation;
import com.awpto.statki.FormationPrototype;

public class FormationRowTest {

	@Test
	public void test() {
		Formation formation = new Formation(new FormationPrototype(10,10));
		assertEquals(formation.getRows(),10);
	}
	
}
