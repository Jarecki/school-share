package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.Ship;
import com.awpto.statki.ShipPrototype;

public class ShipDamagedPosTest {

	@Test
	public void test() {
		Ship ship = new Ship(new ShipPrototype());
		ship.damageAt(0);
		assertEquals(ship.isDamagedAt(0),true);
	}

}
