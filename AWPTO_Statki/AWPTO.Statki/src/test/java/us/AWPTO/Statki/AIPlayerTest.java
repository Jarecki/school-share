package us.AWPTO.Statki;

import static org.junit.Assert.*;

import org.junit.Test;

import com.awpto.statki.AIRandomPlayer;
import com.awpto.statki.Formation;
import com.awpto.statki.FormationPrototype;

public class AIPlayerTest {

	@Test
	public void test() {
		AIRandomPlayer aiRandomPlayer = new AIRandomPlayer(new Formation(new FormationPrototype(10,10)), new Formation(new FormationPrototype(10,10)));
		boolean result = aiRandomPlayer.isAi();
		assertEquals(true, result);
	}

}
