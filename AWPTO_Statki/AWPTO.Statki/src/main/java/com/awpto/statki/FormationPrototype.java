package com.awpto.statki;

import java.awt.Point;
import java.util.ArrayList;

public class FormationPrototype extends FormationBase
{
	private ArrayList<ShipPrototype> ships;
	
	public FormationPrototype(int columns, int rows)
	{
		super(columns, rows);
		ships = new ArrayList<ShipPrototype>();
	}
	
	public void setSize(int columns, int rows)
	{
		if(columns < 1) columns = 1;
		if(rows < 1) rows = 1;
		this.columns = columns;
		this.rows = rows;
		
		for(int i = 0; i < ships.size(); ++i)
			if(!getBounds().contains(ships.get(i).getBounds()))
				ships.remove(i--);
	}
	
	public int getShipCount()
	{
		return ships.size();
	}
	
	public Formation build()
	{
		if(ships.size() == 0)
			throw new RuntimeException("Arena must contain at least 1 ship.");
		return new Formation(this);
	}
	
	public void clear()
	{
		ships.clear();
	}
	
	public ShipPrototype at(int index)
	{
		return ships.get(index);
	}
	
	public ShipPrototype at(Point location)
	{
		return at(location.x, location.y);
	}
	
	public ShipPrototype at(int x, int y)
	{
		int count = ships.size();
		for(int i = 0; i < count; ++i)
			if(ships.get(i).contains(x, y)) return ships.get(i);
		return null;
	}
	
	public boolean tryAdd(ShipPrototype ship)
	{
		if(!getBounds().contains(ship.getBounds()))
			return false;
		
		int count = ships.size();
		for(int i = 0; i < count; ++i)
			if(ship.collidesWith(ships.get(i))) return false;
		
		ships.add(ship);
		return true;
	}
	
	public boolean tryRemoveAt(Point location)
	{
		return tryRemoveAt(location.x, location.y);
	}
	
	public boolean tryRemoveAt(int x, int y)
	{
		int count = ships.size();
		for(int i = 0; i < count; ++i)
		{
			if(ships.get(i).contains(x, y))
			{
				ships.remove(i);
				return true;
			}
		}
		return false;
	}
}
