package com.awpto.statki;

import java.util.Random;

public class RandomFormationCreator
{
	private Random rng;
	private FleetComposer composer;
	
	public RandomFormationCreator(FleetComposer composer)
	{
		if(composer == null)
			throw new NullPointerException("composer");
		
		this.composer = composer;
		rng = new Random();
	}
	
	public Formation generate()
	{
		int columns = composer.formation.getColumns();
		int rows = composer.formation.getRows();
		
		composer.reset();
		while(!composer.isDone())
		{
			boolean horizontal = rng.nextBoolean();
			int x = rng.nextInt(horizontal ? (columns - composer.getCurrentShipLength() + 1) : columns);
			int y = rng.nextInt(horizontal ? rows : (rows - composer.getCurrentShipLength() + 1));
			composer.AddShipAt(x, y, horizontal);
		}
		return composer.formation.build();
	}
}
