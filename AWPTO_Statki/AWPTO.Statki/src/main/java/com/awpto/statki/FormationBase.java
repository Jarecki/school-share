package com.awpto.statki;

import java.awt.Rectangle;

public abstract class FormationBase
{
	protected int columns;
	protected int rows;
	
	protected FormationBase()
	{ }
	
	protected FormationBase(int columns, int rows)
	{
		if(columns < 1) columns = 1;
		if(rows < 1) rows = 1;
		this.columns = columns;
		this.rows = rows;
	}
	
	public int getColumns()
	{
		return columns;
	}
	
	public int getRows()
	{
		return rows;
	}
	
	public int getSize()
	{
		return (columns * rows);
	}
	
	public Rectangle getBounds()
	{
		return new Rectangle(0, 0, columns, rows);
	}
	
	public abstract int getShipCount();
	public abstract ShipBase at(int index);
	
	public HitInfo isHitAt(int x, int y)
	{
		return HitInfo.None;
	}
}
