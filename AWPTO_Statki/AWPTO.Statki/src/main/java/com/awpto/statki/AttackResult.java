package com.awpto.statki;

public enum AttackResult
{
	AlreadyPerformed,
	Missed,
	Hit,
	HitAndDestroyed
}
