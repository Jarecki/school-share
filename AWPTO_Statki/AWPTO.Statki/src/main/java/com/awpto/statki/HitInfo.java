package com.awpto.statki;

public enum HitInfo
{
	None,
	NotHit,
	Hit
}
