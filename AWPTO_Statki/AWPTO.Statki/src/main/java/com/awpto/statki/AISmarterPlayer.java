package com.awpto.statki;

import java.awt.Point;
import java.util.ArrayList;

public class AISmarterPlayer extends AIRandomPlayer
{	
	private ArrayList<ShipInfo> shipInfo;

	public AISmarterPlayer(Formation formation, Formation enemyFormation)
	{
		super(formation, enemyFormation);
		shipInfo = new ArrayList<ShipInfo>();
	}

	public AttackResult performAttack()
	{
		return (shipInfo.size() == 0) ? attackRandom() : attackSmart();
	}
	
	private AttackResult attackRandom()
	{
		Point target = randomTarget();
		AttackResult result = enemyFormation.getHitAt(target);
		
		if(result == AttackResult.Hit)
			shipInfo.add(new ShipInfo(target, enemyFormation));

		return result;
	}
	
	private AttackResult attackSmart()
	{
		AttackResult result = AttackResult.AlreadyPerformed;
		
		while(result == AttackResult.AlreadyPerformed)
		{
			int iShip = rng.nextInt(shipInfo.size());
			ShipInfo info = shipInfo.get(iShip);
			if(info.getDirectionCount() == 0)
			{
				shipInfo.remove(iShip);
				return performAttack();
			}
			
			int iDir = rng.nextInt(info.getDirectionCount());
			Point target = new Point(info.atEdge(iDir));
			
			int direction = info.getDirection(iDir);
			if(direction == 0) --target.x;
			else if(direction == 1) --target.y;
			else if(direction == 2) ++target.x;
			else ++target.y;
			
			result = enemyFormation.getHitAt(target);

			if(result == AttackResult.Hit)
				info.addPoint(iDir, enemyFormation);
			else if(result == AttackResult.HitAndDestroyed)
				shipInfo.remove(iShip);
			else info.removeDirection(iDir);
		}
		return result;
	}
	
	private class ShipInfo
	{		
		private ArrayList<Point> points;
		private ArrayList<Integer> directions;
		
		public ShipInfo(Point p, Formation enemy)
		{
			points = new ArrayList<Point>();
			points.add(p);
			
			directions = new ArrayList<Integer>();
			if(p.x > 0) directions.add(0);
			if(p.y > 0) directions.add(1);
			if(p.x < enemy.getColumns() - 1) directions.add(2);
			if(p.y < enemy.getRows() - 1) directions.add(3);
		}
		
		public Point getLeftMost()
		{
			Point p = points.get(0);
			for(int i = 1; i < getPointCount(); ++i)
				if(points.get(i).x < p.x) p = points.get(i);
			return p;
		}
		
		public Point getRightMost()
		{
			Point p = points.get(0);
			for(int i = 1; i < getPointCount(); ++i)
				if(points.get(i).x > p.x) p = points.get(i);
			return p;
		}
		
		public Point getTopMost()
		{
			Point p = points.get(0);
			for(int i = 1; i < getPointCount(); ++i)
				if(points.get(i).y < p.y) p = points.get(i);
			return p;
		}
		
		public Point getBottomMost()
		{
			Point p = points.get(0);
			for(int i = 1; i < getPointCount(); ++i)
				if(points.get(i).y > p.y) p = points.get(i);
			return p;
		}
		
		public int getPointCount()
		{
			return points.size();
		}
		
		public int getDirectionCount()
		{
			return directions.size();
		}
		
		public int getDirection(int index)
		{
			return directions.get(index);
		}
		
		public Point atEdge(int directionIndex)
		{
			int direction = directions.get(directionIndex);
			if(direction == 0) return getLeftMost();
			if(direction == 1) return getTopMost();
			if(direction == 2) return getRightMost();
			return getBottomMost();
		}
		
		public void removeDirection(int directionIndex)
		{
			directions.remove(directionIndex);
		}
		
		public void addPoint(int directionIndex, Formation enemy)
		{
			int direction = directions.get(directionIndex);
			
			if(direction == 0)
			{
				Point p = getLeftMost();
				points.add(new Point(p.x - 1, p.y));
				if(p.x <= 1) directions.remove((Object)0);
				directions.remove((Object)1);
				directions.remove((Object)3);
			}
			else if(direction == 1)
			{
				Point p = getTopMost();
				points.add(new Point(p.x, p.y - 1));
				if(p.y <= 1) directions.remove((Object)1);
				directions.remove((Object)0);
				directions.remove((Object)2);
			}
			else if(direction == 2)
			{
				Point p = getRightMost();
				points.add(new Point(p.x + 1, p.y));
				if(p.x >= enemy.getColumns() - 2) directions.remove((Object)2);
				directions.remove((Object)1);
				directions.remove((Object)3);
			}
			else
			{
				Point p = getBottomMost();
				points.add(new Point(p.x, p.y + 1));
				if(p.y >= enemy.getRows() - 2) directions.remove((Object)3);
				directions.remove((Object)0);
				directions.remove((Object)2);
			}
		}
	}
}
