package com.awpto.statki.delegates;

import com.awpto.statki.AIRandomPlayer;
import com.awpto.statki.AISmarterPlayer;
import com.awpto.statki.FleetComposer;
import com.awpto.statki.Formation;
import com.awpto.statki.GameTurnManager;
import com.awpto.statki.HumanPlayer;
import com.awpto.statki.PlayerBase;
import com.awpto.statki.RandomFormationCreator;

public final class GameCreator
{
	public static GameTurnManager CreateManager(HumanShipDeployer deployer, boolean smarterAi)
	{
		Formation f1 = deployer.composer.formation.build();
		Formation f2 = new RandomFormationCreator(deployer.composer).generate();
		
		PlayerBase p1 = new HumanPlayer(f1, f2);
		PlayerBase p2 = smarterAi ? new AISmarterPlayer(f2, f1) : new AIRandomPlayer(f2, f1);
		
		return new GameTurnManager(p1, p2);
	}
	
	public static FleetComposer CreateComposer()
	{
		return new FleetComposer(new int[]{ 1, 1, 1, 1, 2, 2, 2, 3, 3, 4, }, 10, 10);
	}
	
	private GameCreator() { }
}
