package com.awpto.statki.ui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.awpto.statki.FleetComposer;
import com.awpto.statki.GameTurnManager;
import com.awpto.statki.NextShipResult;
import com.awpto.statki.TurnResult;
import com.awpto.statki.delegates.GameCreator;
import com.awpto.statki.delegates.HumanAttackHandler;
import com.awpto.statki.delegates.HumanShipDeployer;
import com.awpto.statki.delegates.NextAiTurnHandler;

@SuppressWarnings("serial")
public class GameFrame extends JFrame
{
	private NextAiTurnHandler aiTurnHandler;
	private FormationPanel shipsPanel;
	private FormationPanel hitsPanel;
	private JCheckBox checkHorizontal;
	private JCheckBox checkSmart;
	private JButton buttonNext;
	private JLabel labelInfo;
	private JLabel labelTurn;
	private JLabel labelPlayer;
	private JLabel labelEnemy;
	
	public GameFrame(String title)
	{
		super(title);
		initialize();
		updateStartAnew();
	}
	
	public boolean isHorizontalChecked()
	{
		return checkHorizontal.isSelected();
	}
	
	public void updateNextTurn(GameTurnManager manager, TurnResult result)
	{
		if(result == TurnResult.WrongInput) updateWrongAttack(manager);
		else if(result == TurnResult.GameEnded) updateGameEnded(manager);
		else updatePlayersInfo(manager, result);
	}
	
	private void updateWrongAttack(GameTurnManager manager)
	{
		updatePlayersInfo(manager, TurnResult.WrongInput);
		JOptionPane.showMessageDialog(this, "You have already attacked this tile!");
	}
	
	private void updateGameEnded(GameTurnManager manager)
	{
		hitsPanel.drawShips = true;
		updatePlayersInfo(manager, TurnResult.GameEnded);
		JOptionPane.showMessageDialog(this, manager.hasPlayer1Won() ? "You have won!" : "You have lost!");
		updateStartAnew();
	}
	
	private void updateStartAnew()
	{
		HumanShipDeployer deployer = new HumanShipDeployer(this, GameCreator.CreateComposer());
		
		checkSmart.setSelected(false);
		checkHorizontal.setSelected(true);
		setDeployerInfo(deployer);
		
		shipsPanel.drawHitInfo = false;
		shipsPanel.formation = deployer.composer.formation;
		shipsPanel.setPreferredSize(shipsPanel.getFormationSize());
		shipsPanel.onClick = deployer;
		
		hitsPanel.drawShips = false;
		hitsPanel.drawHitInfo = false;
		hitsPanel.setPreferredSize(shipsPanel.getFormationSize());
		hitsPanel.onClick = null;
		
		aiTurnHandler.manager = null;
		
		buttonNext.setVisible(false);
		labelEnemy.setVisible(false);
		labelTurn.setVisible(false);
		checkHorizontal.setVisible(true);
		checkSmart.setVisible(true);
		hitsPanel.setVisible(false);
		pack();
	}
	
	public void updateComposingFleet(HumanShipDeployer deployer, NextShipResult result)
	{
		switch (result)
		{
			case Failed:
				updateShipAddingFailed();
				break;
				
			case Successful:
				updateShipAddingSuccessful(deployer);
				break;
				
			case Done:
				updateFleetComposed(deployer);
				break;
		}
	}
	
	private void updateShipAddingFailed()
	{
		JOptionPane.showMessageDialog(this, "Invalid ship placement.");
	}
	
	private void updateShipAddingSuccessful(HumanShipDeployer deployer)
	{
		setDeployerInfo(deployer);
		shipsPanel.repaint();
	}
	
	private void updateFleetComposed(HumanShipDeployer deployer)
	{
		labelInfo.setText("All ships have been deployed.");
		setFleetBuildingInfo(deployer.composer);
		shipsPanel.repaint();
		
		if(playerHasStartedTheGame()) updateGameStarted(deployer);
		else updateFleetComposingReset(deployer);
	}
	
	private boolean playerHasStartedTheGame()
	{
		return (JOptionPane.showConfirmDialog(this, "All ships have been deployed. Do you want to start the game?") == JOptionPane.YES_OPTION);
	}
	
	private void updateGameStarted(HumanShipDeployer deployer)
	{
		GameTurnManager manager = GameCreator.CreateManager(deployer, checkSmart.isSelected());
		HumanAttackHandler handler = new HumanAttackHandler(this, manager);
		
		shipsPanel.formation = manager.getPlayer1().formation;
		shipsPanel.drawHitInfo = true;
		shipsPanel.onClick = null;
		
		hitsPanel.formation = manager.getPlayer2().formation;
		hitsPanel.drawHitInfo = true;
		hitsPanel.onClick = handler;
		
		aiTurnHandler.manager = manager;
		updatePlayersInfo(manager, null);
		
		buttonNext.setVisible(true);
		labelEnemy.setVisible(true);
		labelTurn.setVisible(true);
		checkHorizontal.setVisible(false);
		checkSmart.setVisible(false);
		hitsPanel.setVisible(true);
		pack();
	}	
	
	private void updateFleetComposingReset(HumanShipDeployer deployer)
	{
		deployer.composer.reset();
		setDeployerInfo(deployer);
		shipsPanel.repaint();
	}
	
	private void updatePlayersInfo(GameTurnManager manager, TurnResult result)
	{
		labelPlayer.setText("Player's ships left: " + manager.getPlayer1().formation.shipsAlive());
		labelEnemy.setText("Enemy's ships left: " + manager.getPlayer2().formation.shipsAlive());
		
		setTurnInfo(manager, result);
		
		buttonNext.setEnabled(manager.getCurrent().isAi());
		shipsPanel.repaint();
		hitsPanel.repaint();
	}
	
	private void setTurnInfo(GameTurnManager manager, TurnResult result)
	{
		labelTurn.setText(manager.isPlayer1Turn() ? "Player's turn" : "Enemy's turn");
		
		if(result == null)
			labelInfo.setText("The game has started.");
		else if(result == TurnResult.PlayersSwitched)
			labelInfo.setText(manager.isPlayer1Turn() ? "The enemy has missed." : "You have missed.");
		else if(result == TurnResult.PlayerContinues)
			labelInfo.setText(manager.isPlayer1Turn() ? "You have hit the enemy's ship!" : "Your ship has been hit by the enemy!");
		else if(result == TurnResult.PlayerDestroyedShip)
			labelInfo.setText(manager.isPlayer1Turn() ? "You have destroyed the enemy's ship!" : "Your ship has been destroyed by the enemy!");
		else if(result == TurnResult.GameEnded)
			labelInfo.setText("The game has ended.");
	}

	private void setDeployerInfo(HumanShipDeployer deployer)
	{
		setShipDeploymentInfo(deployer.composer);
		setFleetBuildingInfo(deployer.composer);
	}
	
	private void setShipDeploymentInfo(FleetComposer composer)
	{
		labelInfo.setText("Deploy ship no. " + (composer.getShipPointer() + 1) + " (length = " + composer.getCurrentShipLength() + ")");
	}
	
	private void setFleetBuildingInfo(FleetComposer composer)
	{
		labelPlayer.setText("Build your fleet (" + composer.getShipPointer() + " / " + composer.getShipCount() +  "):");
	}
	
	private void initialize()
	{
		GridBagLayout gbl = new GridBagLayout();
		setLayout(gbl);
		
		checkHorizontal = new JCheckBox();
		checkHorizontal.setText("horizontal");
		setComponentGridPos(checkHorizontal, gbl, 2, 11, 1, 1);
		
		checkSmart = new JCheckBox();
		checkSmart.setText("smarter AI");
		setComponentGridPos(checkSmart, gbl, 9, 0, 1, 1);
		
		labelInfo = new JLabel();
		setComponentGridPos(labelInfo, gbl, 0, 11, 2, 1);
		
		labelTurn = new JLabel();
		setComponentGridPos(labelTurn, gbl, 12, 11, 2, 1);
		
		labelPlayer = new JLabel();
		setComponentGridPos(labelPlayer, gbl, 0, 0, 5, 1);
		
		labelEnemy = new JLabel();
		setComponentGridPos(labelEnemy, gbl, 10, 0, 5, 1);
		
		buttonNext = new JButton();
		buttonNext.setText("Next AI Turn");
		aiTurnHandler = new NextAiTurnHandler(buttonNext, this);
		buttonNext.addMouseListener(aiTurnHandler);
		setComponentGridPos(buttonNext, gbl, 11, 11, 1, 1);
		
		shipsPanel = new FormationPanel();
		setComponentGridPos(shipsPanel, gbl, 0, 1, 10, 10);
		
		hitsPanel = new FormationPanel();		
		setComponentGridPos(hitsPanel, gbl, 10, 1, 10, 10);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}
	
	private void setComponentGridPos(Component c, GridBagLayout gbl, int x, int y, int w, int h)
	{
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = w;
		gbc.gridheight = h;
		gbc.insets = new Insets(10, 10, 10, 10);
		gbl.setConstraints(c, gbc);
		add(c);
	}
}
