package com.awpto.statki;

public class FleetComposer
{
	private int[] shipLengths;
	private int shipPointer;
	public final FormationPrototype formation;
	
	public FleetComposer(int[] shipLengths, int columns, int rows)
	{
		if(shipLengths == null)
			throw new NullPointerException("shipLengths");
		
		if(shipLengths.length == 0)
			throw new RuntimeException("ship count must be greater than 0.");
		
		if(columns < 1) columns = 1;
		if(rows < 1) rows = 1;
		
		int fullCount = 0;
		for(int i = 0; i < shipLengths.length; ++i)
		{
			if(shipLengths[i] < 1 || (shipLengths[i] > columns && shipLengths[i] > rows))
				throw new RuntimeException("ship's length must be greater than 0 and less than or equal to either column or row count.");
			fullCount += shipLengths[i];
		}
		
		if(fullCount > columns * rows)
			throw new RuntimeException("fleet is too large.");
		
		formation = new FormationPrototype(columns, rows);
		this.shipLengths = shipLengths.clone();
		shipPointer = 0;
	}
	
	public FleetComposer(FleetComposer other)
	{
		formation = new FormationPrototype(other.formation.getColumns(), other.formation.getRows());
		shipLengths = other.shipLengths;
		shipPointer = 0;
	}
	
	public void reset()
	{
		shipPointer = 0;
		formation.clear();
	}
	
	public int getShipCount()
	{
		return shipLengths.length;
	}
	
	public int getShipPointer()
	{
		return shipPointer;
	}
	
	public int getCurrentShipLength()
	{
		return shipLengths[shipPointer];
	}
	
	public NextShipResult AddShipAt(int x, int y, boolean horizontal)
	{
		if(isDone()) return NextShipResult.Done;
		
		ShipPrototype ship = new ShipPrototype();
		ship.setLocation(x, y);
		ship.setLength(getCurrentShipLength());
		ship.setOrientation(horizontal);
		
		if(formation.tryAdd(ship)) 
		{
			++shipPointer;
			return isDone() ? NextShipResult.Done : NextShipResult.Successful;
		}
		return NextShipResult.Failed;
	}
	
	public boolean isDone()
	{
		return (shipPointer == shipLengths.length);
	}
}
